<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ID: 1
        DB::table('productos')->insert([        	
        	'nombre'		   		=> 'Producto 1',        	
            'precio'	   		=> 500,
            'prodtag'	   		=> 'VGA1',
            'tipo_id'	   		=> 1,                
        ]);

        //ID: 2
        DB::table('productos')->insert([        	
        	'nombre'		   		=> 'Producto 2',        	
            'precio'	   		=> 300,
            'prodtag'	   		=> 'HDD2',
            'tipo_id'	   		=> 3,                
        ]);

        //ID: 3
        DB::table('productos')->insert([        	
        	'nombre'		   		=> 'Producto 3',        	
            'precio'	   		=> 150,
            'prodtag'	   		=> 'CPU3',
            'tipo_id'	   		=> 4,                
        ]);

        //ID: 4
        DB::table('productos')->insert([        	
        	'nombre'		   		=> 'Producto 4',        	
            'precio'	   		=> 200,
            'prodtag'	   		=> 'VGA4',
            'tipo_id'	   		=> 1,                
        ]);
    }
}
