<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call('TipoSeeder');
        $this->call(TipoSeeder::class);
        $this->call(ProductoSeeder::class);
    }
}
