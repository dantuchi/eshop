<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ID: 1
        DB::table('tipos')->insert([        	
        	'nombre'		   		=> 'Placa de Video',        	
            'tag'	   		=> 'VGA',            
        ]);

        //ID: 2
        DB::table('tipos')->insert([        	
        	'nombre'		   		=> 'Memoria Ram',        	
            'tag'	   		=> 'RAM',            
        ]);

        //ID: 3
        DB::table('tipos')->insert([        	
        	'nombre'		   		=> 'Disco Duro',        	
            'tag'	   		=> 'HDD',            
        ]);

        //ID: 4
        DB::table('tipos')->insert([        	
        	'nombre'		   		=> 'Procesador',        	
            'tag'	   		=> 'CPU',            
        ]);

        //ID: 5
        DB::table('tipos')->insert([        	
        	'nombre'		   		=> 'Fuente',        	
            'tag'	   		=> 'FTE',            
        ]);
    }
}
