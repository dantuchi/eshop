<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;


    protected $fillable = ['nombre','precio'];

    public function tipo()
    {
    	return $this->belongsTo('App\Models\Tipo','tipo_id');
    }

}
