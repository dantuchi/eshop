@extends('principal')

@section('content')

<div class="container">
    <div class="row d-flex flex-row">
        <h3 class="text-primary">Listado de Productos</h3>
    </div>
    <hr>

    <div class="row py-2">
		<div class="col-md-12">
			

				<div class="card-body">
					<p class="alert alert-info d-none d-sm-block" >
					    Desde aquí, podrá <strong>crear, editar</strong> o <strong>dar de baja</strong> un Producto.
					</p>

					<table class="table table-bordered table-hover table-responsive-sm shadow">
						<thead class="bg-dark text-white">
							<tr>
								<th>Nombre</th>
								<th>Clave de Producto</th>
								<th>Tipo</th>
								<th>Precio</th>
																
								<th>
									<div class="d-flex justify-content-center">
										<a href="#" >
					            			<button class="btn btn-primary btn-circle" type="button"><i class="fa fw fa-plus"></i></button>
					            		</a>
				            		</div>
								</th>
							</tr>
						</thead>
						<tbody>
							@forelse($productos as $prod)
							<tr>
								<td>{{ $prod->nombre }}</td>
								<td>{{ $prod->prodtag }}</td>
								<td>{{ $prod->tipo->nombre }}</td>
								<td>{{ $prod->precio }}</td>
								<td>
									<div class="d-flex justify-content-center">

										<a href="#"><button class="btn btn-info btn-sm btn-circle mr-2"><i class="fa fw fa-info"></i></button></a>
																			
										<a href="#"><button class="btn btn-warning btn-sm btn-circle mr-2"><i class="fas fa-edit"></i></button></a>
									
									</div>
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="6"><p>No existen Equipos registrados</p></td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			
		</div>
	</div>
    




</div>    


@endsection