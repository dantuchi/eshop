<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>E-shop informática</title>

        @include('layouts.css')

    </head>
    <body class="sb-nav-fixed">
        @include('layouts.nav')

        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                @include('layouts.sidebar')
            </div>

            <!-- ACA IRIA EL CONTENIDO -->

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <div class="py-2"></div>
                        @yield('content')
                    </div>
                </main>
                
            </div>
        </div>

        @include('layouts.scripts')
    </body>
</html>
