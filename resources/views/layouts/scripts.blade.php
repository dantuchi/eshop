<script src="{{ asset('js/jquery.js') }}" ></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}" ></script>

<script type="text/javascript" href="{{ asset('sbadmin/dist/js/scripts.js') }}"></script>
<script type="text/javascript" href="{{ asset('sbadmin/src/js/scripts.js') }}"></script>
<script type="text/javascript" href="{{ asset('fontawesome/js/all.js') }}"></script>

<script type="text/javascript">
    $("#sidebarToggle").on("click", function(e) {
    e.preventDefault();
    $("body").toggleClass("sb-sidenav-toggled");
    });
</script>